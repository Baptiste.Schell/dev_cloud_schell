# Projet dev cloud

### Sommaire 

- [Dépendances](#dépendances)
- [Questions](#questions)

### Dépendances

- Docker
- Kubernetes
- Flack (pour les apis python)

### Questions

> __Quelle est la différence entre un service ClusterIP et NodePort sous Kubernetes ?__  
>
>
> Un service ClusterIP sous Kubernetes est utilisé pour exposer un service uniquement à l'intérieur du cluster. Cela signifie que les pods et autres services peuvent communiquer avec lui, mais il n'est pas accessible directement depuis l'extérieur du cluster. En revanche, un service NodePort expose le service à l'extérieur du cluster en mappant un port du service à un port sur chaque nœud du cluster. Cela permet d'accéder au service en utilisant l'IP de n'importe quel nœud et le NodePort configuré. Le choix entre ces deux types dépend des besoins d'accessibilité de l'application.  

<br>

> __Quel type de scaling choisiriez-vous pour une base de données (BDD) ? Et pourquoi ?__  
>
>
> Pour le scaling d'une base de données (BDD), je choisirais le scaling vertical plutôt que le scaling horizontal. En effet, les bases de données relationnelles, qui nécessitent souvent une forte consistance et des transactions ACID, sont généralement plus adaptées à une augmentation des ressources d'une seule instance (CPU, RAM) plutôt qu'à la répartition de la charge sur plusieurs instances. Le scaling horizontal est plus complexe à mettre en œuvre en raison des défis de réplication et de partitionnement des données.

<br>

> __Pour quelles raisons choisir un cloud privé plutôt que public ?__
> 
>
> Choisir un cloud privé plutôt que public peut être motivé par plusieurs raisons, notamment la sécurité, le contrôle des données, et les exigences de conformité. Un cloud privé permet de garder un contrôle total sur l'infrastructure et les données, ce qui est crucial pour les entreprises traitant des informations sensibles ou soumises à des régulations strictes. De plus, un cloud privé peut offrir des performances plus prévisibles et personnalisables pour des besoins spécifiques.  

<br>

> __Dans quel cas prendre une instance AWS réservée ? Et quel est l'intérêt ?__
> 
> 
> Prendre une instance AWS réservée est pertinent lorsque l'on sait que l'application ou le service aura une charge stable et continue sur une longue période. L'intérêt des instances réservées réside dans les économies substantielles qu'elles offrent par rapport à l'utilisation d'instances à la demande. En s'engageant sur un ou trois ans, on peut réduire significativement les coûts, ce qui est avantageux pour les entreprises avec des workloads prévisibles et constants, comme les environnements de production stables.