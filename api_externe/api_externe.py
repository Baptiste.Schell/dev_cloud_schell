from flask import Flask, jsonify
import requests

app = Flask(__name__)

@app.route('/api/test', methods=['GET'])
def call_api1():
    response = requests.get('http://api1:5000/api/info')
    data = response.json()
    return jsonify({"message": "This is information from API 2", "api1_data": data})

@app.route('/api/extra', methods=['GET'])
def get_extra():
    return jsonify({"message": "This is extra information from API 2"})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)